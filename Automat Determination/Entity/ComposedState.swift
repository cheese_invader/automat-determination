//
//  ComposedState.swift
//  Automat Determination
//
//  Created by Marty on 18/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct ComposedState {
    private var stateByAlias = [composedStateType: Int]()
    private var aliasByState = [Int: composedStateType]()
    
    func contains(alias: Int) -> Bool {
        return aliasByState[alias] != nil
    }
    
    func contains(composedState: composedStateType) -> Bool {
        return stateByAlias[composedState] != nil
    }
    
    mutating func insertSet(_ set: composedStateType, byAlias alias: Int) {
        stateByAlias[set] = alias
        aliasByState[alias] = set
    }
    
    func composedStateByAlias(_ alias: Int) -> composedStateType? {
        return aliasByState[alias]
    }
    
    func aliasByComposedState(_ state: composedStateType) -> Int? {
        return stateByAlias[state]
    }
}
