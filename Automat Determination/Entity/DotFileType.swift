//
//  DotFileType.swift
//  Automat Determination
//
//  Created by Marty on 18/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

enum DotFileType: String {
    case input = "in.dot"
    case output = "out.dot"
}
