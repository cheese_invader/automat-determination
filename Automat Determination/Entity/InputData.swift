//
//  InputData.swift
//  Automat Determination
//
//  Created by Marty on 18/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

struct InputData {
    var x: Int
    var z: Int
    var k: Int
    
    var finalStates: [Int]
    var transitionTable: transitionTableType
    

    init?(fromString str: String) {
        var components = str.components(separatedBy: "\n")
        components.removeLast()
        
        guard let x = Int(components[0]) else {
            return nil
        }
        guard let z = Int(components[1]) else {
            return nil
        }
        guard let k = Int(components[2]) else {
            return nil
        }
        self.x = x
        self.z = z
        self.k = k
        self.finalStates = []
        self.transitionTable = Array(repeating: Array(repeating: [], count: x), count: z)
        
        let finalStates = components[3].components(separatedBy: " ")
        
        guard finalStates.count == k else {
            return nil
        }
        
        for finalState in finalStates {
            guard let state = Int(finalState) else {
                return nil
            }
            self.finalStates.append(state)
        }
        
        let transitions = components[4...]
        
        guard transitions.count == z else {
            return nil
        }
        
        for (fromZ, transition) in transitions.enumerated() {
            if transition.isEmpty {
                continue
            }
            let transitionComponents = transition.components(separatedBy: " ")
            guard transitionComponents.count % 2 == 0 else {
                return nil
            }
            
            for index in stride(from: transitionComponents.startIndex, to: transitionComponents.endIndex, by: 2) {
                guard let transitionZ = Int(transitionComponents[index]),
                      let transitionX = Int(transitionComponents[index + 1]) else {
                    return nil
                }
                
                self.transitionTable[fromZ][transitionX].insert(transitionZ)
            }
        }
    }
    
    func toDot() -> String {
        var nodes = ""
        
        for i in stride(from: 0, to: z, by: 1) {
            if finalStates.contains(i) {
                nodes.append("\(i) [shape=box]\n")
            } else {
                nodes.append("\(i)\n")
            }
        }
        
        var transitions = ""
        
        for (stateFrom, statesList) in transitionTable.enumerated() {
            for (x, composedState) in statesList.enumerated() {
                for stateTo in composedState {
                    let transition = getDotTransition(from: stateFrom, to: stateTo, label: x)
                    transitions.append(transition)
                }
            }
        }
        
        return "digraph Deterministic {\n\(nodes)\(transitions)}"
    }
    
    private func getDotTransition(from: Int, to: Int, label: Int) -> String {
        return "\t\(from)->\(to) [label=\(label)]\n"
    }
}
