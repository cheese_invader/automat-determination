//
//  OutputData.swift
//  Automat Determination
//
//  Created by Marty on 18/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct OutputData {
    var x: Int
    var z: Int
    var k: Int
    
    var finalStates: [Int]
    
    // first: Signal, last: State
    var transitionTable: [[Int]]
    
    func toString() -> String {
        var outString = ""
        outString.append("\(x)\n")
        outString.append("\(z)\n")
        outString.append("\(k)\n")
        
        for (i, state) in finalStates.enumerated() {
            outString.append("\(state)")
            if i != finalStates.count - 1 {
                outString.append(" ")
            }
        }
        
        outString.append("\n")
        
        for (x, xLine) in transitionTable.enumerated() {
            for (zFrom, zTo) in xLine.enumerated() {
                outString.append("\(zTo)")
                if zFrom != xLine.count - 1 {
                    outString.append(" ")
                }
            }
            if x != transitionTable.count - 1 {
                outString.append("\n")
            }
        }
        
        outString.append("\n")
        
        return outString
    }
    
    func toDot() -> String {
        var nodes = ""
        
        for i in stride(from: 0, to: z, by: 1) {
            if finalStates.contains(i) {
                nodes.append("\(i) [shape=box]\n")
            } else {
                nodes.append("\(i)\n")
            }
        }
        
        var transitions = ""
        
        for (x, xLine) in transitionTable.enumerated() {
            for (zFrom, zTo) in xLine.enumerated() where zTo != -1 {
                let transition = getDotTransition(from: zFrom, to: zTo, label: x)
                transitions.append(transition)
            }
        }
        
        return "digraph Deterministic {\n\(nodes)\(transitions)}"
    }
    
    private func getDotTransition(from: Int, to: Int, label: Int) -> String {
        return "\t\(from)->\(to) [label=\(label)]\n"
    }
}
