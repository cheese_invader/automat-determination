//
//  TableKey.swift
//  Automat Determination
//
//  Created by Marty on 18/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

struct TableKey: Hashable {
    var stateFrom: Int
    var signal: Int
}
