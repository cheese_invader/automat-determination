//
//  Determinator.swift
//  Automat Determination
//
//  Created by Marty on 18/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Cocoa

class Determinator {
    private var stateManager = ComposedState()
    private let inputData: InputData
    
    private var finalStates: Set<Int>
    private var statesQueue = [0]
    private var visitedStates = Set<Int>()
    
    private var table = transitionTableType()
    
    
    
    // MARK: - initializer -
    init(data: InputData) {
        inputData = data
        table = data.transitionTable
        finalStates = Set(data.finalStates)
    }

    
    func determinate() -> OutputData {
        while !statesQueue.isEmpty {
            let state = statesQueue.removeFirst()
            goThroughState(state)
        }
        
        let newStates = visitedStates.sorted()//listOfUsefullStates()
        let alias = getTypealiasForStates(newStates)
        finalStates.formIntersection(newStates)
        var newFinaleStates = Set<Int>()
        
        for finalState in finalStates {
            newFinaleStates.insert(alias[finalState] ?? 0)
        }
        
        let minimized = minimize(usefullStates: newStates, alias: alias)
        
        return OutputData(x: inputData.x, z: newStates.count, k: newFinaleStates.count, finalStates: newFinaleStates.sorted(), transitionTable: minimized)
    }
    
    // MARK: - Helpers -
    private func goThroughState(_ z: Int) {
        visitedStates.insert(z)
        
        for (x, composedState) in table[z].enumerated() {
            if composedState.count == 1 {
                let stateToGo = composedState.first!
                if !visitedStates.contains(stateToGo) && !statesQueue.contains(stateToGo) {
                    statesQueue.append(stateToGo)
                }
                continue
            } else if composedState.count == 0 {
                continue
            }
            
            let originsOfComposed = getOriginStatesByComposedState(composedState)
            
            if let newState = stateManager.aliasByComposedState(originsOfComposed) {
                table[z][x] = [newState]
            } else {
                let newState = composeState(originsOfComposed)
                statesQueue.append(newState)
                table[z][x] = [newState]
            }
        }
    }
    
    private func composeState(_ composedState: composedStateType) -> Int {
        let currentState = table.count
        
        if !finalStates.intersection(composedState).isEmpty {
            finalStates.insert(currentState)
        }
        
        stateManager.insertSet(composedState, byAlias: currentState)
        table.append(Array(repeating: [], count: inputData.x))
        
        for z in composedState {
            for x in stride(from: 0, to: inputData.x, by: 1) {
                let appendingOriginComponents = getOriginStatesByComposedState(table[z][x])
                table[currentState][x].formUnion(appendingOriginComponents)
            }
        }
        
        return currentState
    }
    
    private func getOriginStatesByAlias(_ z: Int) -> Set<Int> {
        if z < inputData.z {
            return [z]
        }
        var origins = Set<Int>()
        
        let firstLayer = stateManager.composedStateByAlias(z) ?? []
        
        for state in firstLayer {
            origins.formUnion(getOriginStatesByAlias(state))
        }
        
        return origins
    }
    
    private func getOriginStatesByComposedState(_ composedState: composedStateType) -> Set<Int> {
        var origins = Set<Int>()
        
        for z in composedState {
            origins.formUnion(getOriginStatesByAlias(z))
        }
        
        return origins
    }
    
    private func getTypealiasForStates(_ states: [Int]) -> [Int: Int] {
        var alias = [Int: Int]()
        for (i, state) in states.enumerated() {
            alias[state] = i
        }
        return alias
    }
    
    private func minimize(usefullStates: [Int], alias: [Int: Int]) -> [[Int]] {
        var newTable = Array(repeating: Array(repeating: -1, count: usefullStates.count), count: inputData.x)
        
        for state in usefullStates {
            for (x, val) in table[state].enumerated() where !val.isEmpty {
                let value = val.first!
                newTable[x][alias[state]!] = alias[value]!
            }
        }
        
        return newTable
    }
}
