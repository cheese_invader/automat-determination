//
//  FileStreaming.swift
//  Automat Determination
//
//  Created by Marty on 18/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Cocoa

private let  inputFileName = "in.txt"
private let outputFileName = "out.txt"


class FileStreaming {
    func readFile() -> String? {
        let fileURL = URL(fileURLWithPath: inputFileName)
        
        guard let text = try? String(contentsOf: fileURL) else {
            return nil
        }
        
        return text
    }
    
    func writeFile(_ text: String) {
        let fileURL = URL(fileURLWithPath: outputFileName)
        
        try? text.write(to: fileURL, atomically: false, encoding: .utf8)
    }
    
    func writeDotFile(_ text: String, file: DotFileType) {
        let fileURL = URL(fileURLWithPath: file.rawValue)
        
        try? text.write(to: fileURL, atomically: false, encoding: .utf8)
    }
}
