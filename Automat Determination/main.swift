//
//  main.swift
//  Automat Determination
//
//  Created by Marty on 18/11/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

import Foundation

// first: State, last: Signal
typealias transitionTableType = [[composedStateType]]
typealias composedStateType = Set<Int>


let fs = FileStreaming()
let drawer = DotDrawing()

guard let rawData = fs.readFile() else {
    print("Somthing was wrong while reading data")
    fatalError()
}

guard let data = InputData(fromString: rawData) else {
    print("Somthing was wrong with the data")
    fatalError()
}

let determinator = Determinator(data: data)
let output = determinator.determinate()

fs.writeFile(output.toString())

fs.writeDotFile(data.toDot(), file: .input)
fs.writeDotFile(output.toDot(), file: .output)

drawer.drawDots()
